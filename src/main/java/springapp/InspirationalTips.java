package springapp;

public class InspirationalTips implements TipsService{
    @Override
    public String getTip() {
        return "Keep calm and you will be able to handle storms";
    }
}
