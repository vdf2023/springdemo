package springapp;

public class MathTeacher implements Teacher, TipsService{

    private TipsService tipsService;

    public MathTeacher(TipsService tipsService) {
        this.tipsService = tipsService;
    }

    @Override
    public String getExercise(){
        return "what is 2 + 2 result?";
    }

    @Override
    public String getTip(){
        return tipsService.getTip();
    }
}
