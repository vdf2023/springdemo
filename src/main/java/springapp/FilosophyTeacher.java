package springapp;

public class FilosophyTeacher implements Teacher, TipsService {

    private TipsService tipsService;

    public FilosophyTeacher(TipsService tipsService) {
        this.tipsService = tipsService;
    }

    @Override
    public String getExercise(){
        return "Who am I?";
    }

    @Override
    public String getTip() {
        return tipsService.getTip();
    }
}
